\PassOptionsToPackage{hidelinks,unicode,linktoc=all}{hyperref}
\PassOptionsToPackage{hyphens}{url}

\documentclass[ 15pt, twocolumn, fleqn]{report}

\usepackage[english]{babel}
\usepackage[T1]{fontenc}
\usepackage[margin=7em]{geometry}
\usepackage{microtype}
\DisableLigatures[?,!]{encoding=T1}

\usepackage{rutitlepage}
\usepackage{pdfpages}
\usepackage{lipsum}
\usepackage{hyperref}
\usepackage{nopageno}
\usepackage{graphicx}
\usepackage{parskip}

\begin{document}

\title{Session prediction using shingling-indexed matrixes}
\author{Niek Janssen}
\date{\today}

\maketitleru[
    others={{Supervisor:}{Arjen de Vries},{Second reader:}{Djoerd Hiemstra}},
    course={Research internship}
]

\section*{Abstract}
\textbf{In the current world of search engines, users usually need multiple
search queries to find what they want. For search engines, knowing which queries
belong to the same search session is valuable in order to generate the best
possible results. }

\textbf{In this paper, we look at a statistical way to determine whether two
queries belong to the same session. We look at matrices marking statistical
likeliness that two shinglings belong to queries of the same session, and
perform statistical analyses on them to make predictions.}

\section*{Introduction}
When users use a search engine, a sequence of queries working to a single goal
is called a search \textit{session}. For search engines, knowing which queries
belong to the same session and which queries start a new session, is very
valuable in order to generate the best results possible. This way, the search
engines can build a context of what the user is searching for, and specifically
search for new results in this context. 

In this short paper, we are going to look at a statistical way to determine
whether two queries belong to the same session. We first look into
shinglings, and how we can use those to present queries in a simpler form. We
use this simpler form to construct a matrix containing our prediction
model. Secondly, we discuss methods to train and use this matrix. Thirdly,
we discuss the performance of these methods. And finally, we discuss
further possible improvements on this method.

The source code of the experiment can be found at \cite{code}. 

\section*{A matrix of shinglings}
The first general idea is to construct a matrix, where each row and column
correspond to a query. 
The cells of the matrix contain the likelihood of the two queries on the
axis belonging to the same session. We first have to train this matrix to fill
it with the correct likelihoods. After that, whenever we want to know whether
two queries are in the same session, we just look up the likelihood
value in the matrix, and compare it to a certain threshold value. 

A big problem to tackle is the size of this matrix. To compare all unique
queries with all unique queries, the matrix would be huge. Additionally, the
amount of training data needed is even bigger, as we want the statistical
relevance for all of these individual cells to be sufficient. 

To remedy this issue, we use shinglings to index this matrix. Any string
$S$ is transformable into a set of $n$-shinglings($S$). This set contains all
possible substrings of $S$ of length $n$. For example, the set of 
$3$-shinglings(`World!`) is $\{$`Wor`, `orl`, `rld`, `ld!`$\}$. 

Because the matrix would be mirrored along its diagonal, we can make it
a triangular matrix, halving its size. Finally, we limit the characters in
the shinglings to a-z and 0-9. Using these techniques together, a matrix of
16-bit cell sizes would be 2.2GB for 3-shinglings, and 2.8TB for 4-shinglings.
This makes 3-shinglings a good choice for experimental setups, while bigger
datacenters may be able to handle 4-shingling matrixes. 

\section*{Training the matrix}
\begin{figure*}
    \includegraphics[width=\textwidth]{graph}
    \caption{Scores of query pairs in the same session (green) or different
    sessions (red)}
    \label{confusion-graph}
\end{figure*}

To train the matrix, we want to obtain shingling pairs that are statistically
likely to occur in a query pair that belongs to the same session. For this, we
first need to obtain the queries belonging to the same session. We can use a
relatively inaccurate measure, as long as it statistically has a significant
higher chance of being correct than incorrect. 

To transform a pair of queries into pairs of shinglings, we simply take the
cartesian product of the two sets of shinglings extracted from the two queries:
For a pair $(q_1, q_2)$ of queries, the shingling pairs are computed as
$n$-shinglings$(q_1) \times n$-shinglings$(q_2)$. This way, one pair of queries
results in many different pairs of shinglings, depending on the length of the
query. 

Note that our matrix only supports shinglings with lowercase alphanumeric
characters. Before extracting shinglings, diacritics should be removed, 
uppercase characters should be lowercased, and all other characters should be
removed. Finally, we use stemming and stopping to simplify the queries. 

We now count the shingling pairs we obtained using this method in their
respective cells in the matrix.  After training, shingling pairs in our matrix
with a higher chance to occur in two queries of the same session now should have
a higher count than shinglings that do not occur.

However, there is one problem.  Shinglings that have a lesser overall occurrence
in queries, have a lower score as well. We can mitigate this by a simple
normalization step: we divide the score of each cell by the product of the total
occurrences of the shinglings on its axes. 

\section*{Session prediction}
Testing whether a pair of queries belongs to the same session is done in a
similar way to training the matrix. Firstly, we extract pairs of shinglings in
the same manner as we did in the training phase. Secondly, we look up the scores
in the matrix, obtaining a set of scores. Lastly, we aggregate these scores by
averaging them, resulting in a single final score. This score can then be
compared to a threshold value, yielding a boolean value indicating whether the
two queries belong to the same session. 

\section*{Experimental setup}
The process described above has been implemented \cite{code} using typescript
\cite{typescript} and NodeJs \cite{nodejs}. We use two different datasets to
evaluate our approach. 

Firstly, we have a Bing query dataset that contains 15 million queries,
including HTTP session IDs and timestamps. This dataset is ideal to train the
matrix, as it is a large amount of recorded queries, and the combination of HTTP
session ID and timestamp can be used to create our inaccurate measure. We define
this inaccurate measure as: Two queries belong to the same session \textit{iff} the
HTTP session ID's are equal, and the timestamps have a difference of at most $t$. 

Secondly, we have the TREC 2014 session track dataset \cite{trec}. It contains recorded
search sessions of users who were given a random task. This dataset is ideal to
run tests on the trained matrix, as the recording of the search sessions means
we can verify the results we get from our session prediction algorithm. 

Due to size constraints, we use a matrix of $3$-shinglings

\section*{Results}
Using the methods described above, we trained a matrix on the Bing dataset. A
time difference of $t=5$ minutes was used. We then computed the scores
of 10,000 query pairs from the same session, and 10,000 query pairs from
different sessions. These query pairs have been randomly selected in the TREC
2014 data. 

Figure \ref{confusion-graph} shows the distribution of
scores of these query pairs. The green line shows the scores of queries
belonging to the same session, the red line shows the scores of queries not
belonging to the same session. 

From this data, we can compute a threshold value yielding optimal results. From
this threshold value, we can create a confusion matrix. 
Table \ref{stats} holds some interesting statistics over these results. Table
\ref{confusion-table} holds the confusion matrix over the experiment. 

\begin{table}
    \caption{Some statistics over the trained matrix}
    \label{stats}
    \begin{tabular}{ll}
        Matrix density & 3.88\% \\
        Computed threshold value: & 924 \\
        Accuracy: & 73\% \\
    \end{tabular}
\end{table}

\begin{table}
    \caption{The confusion matrix for the results of session prediction}
    \label{confusion-table}
    \begin{tabular}{lrr}
        & \textbf{predicted no} & \textbf{predicted yes} \\
        \textbf{actual no} & 82\% & 18\% \\
        \textbf{actual yes} & 38\% & 62\% \\
    \end{tabular}
\end{table}

These results show that the accuracy of this method is about 73\%. The confusion
matrix shows that this method can recognize query pairs not belonging to the same
session more accurately (82\%) then query pairs that actually belong to the same
session (62\%). 

\section*{Results integrity}
In the experiment above, the matrix density is only 3.88\%. It is possible that
there are many shinglings that do not occur in the English language, but it is
also possible that we actually have too little data to properly train the
matrix. To verify this, we can repeat the experiment with matrixes trained on
less data, and compare the results. 

Table \ref{data-variety} compares the number of millions of queries used to
train the matrix to the accuracy, the confusion matrix, and the matrix density.
Note that for this graph, a maximum time difference of $t=1$ minute has been
used. 

\begin{table}
    \caption{Differences in accuracy when training with less data}
    \label{data-variety}
    \begin{tabular}{rr|rrrr|r}
        && \multicolumn{4}{c|}{\textbf{Confusion matrix}} \\
    \textbf{queries} & \textbf{acc} & \textbf{n/n} & \textbf{y/n} & \textbf{n/y} & \textbf{y/y} & \textbf{dens} \\
    \hline
    15 mln.& 72 & 82 & 18 & 40 & 60 & 2.95\\
    12 mln.& 72 & 82 & 18 & 36 & 64 & 2.63\\
    9 mln.& 73 & 84 & 16 & 36 & 64 & 2.26 \\
    6 mln.& 71 & 84 & 16 & 44 & 56 & 1.74\\
    3 mln.& 70 & 84 & 16 & 42 & 58 & 1.20\\
    \end{tabular}
\end{table}

It is interesting to see how the accuracy holds up pretty well, even though we
only use a fraction of the data available to train on. The density of the
matrix, however, shringks fast. This means that the amounts of wrongly-added
shingling pairs shrinks/grows as fast as the amounts of rightly added shingling
pairs. 

Another metric we can verify, is the time $t$ used during training, indicating
which queries belong to the same session according to our inaccurate measure.
One could argue, that by decreasing this value, less data will be used in the
computations. However, the data that is used, will be more accurate, as the
chances of these queries belonging to the same session theoretically increases.
As we have seen in table \ref{data-variety}, the amount of data only plays a
minimal role in the accuracy of the data. So, our expectations are that the
accuracy should increase by decreasing time $t$. Table \ref{time-variety} shows
the results. 

\begin{table}
    \caption{Differences in accuracy when training different $t$}
    \label{time-variety}
    \begin{tabular}{rrrrrr}
        && \multicolumn{4}{c}{\textbf{Confusion matrix}} \\
    \textbf{time} & \textbf{acc} & \textbf{n/n} & \textbf{y/n} & \textbf{n/y} & \textbf{y/y} \\
    \hline
    0.5 & 71 & 42 & 8 & 21 & 29 \\
    1.0 & 72 & 42 & 8 & 20 & 30 \\
    2.0 & 72 & 41 & 9 & 19 & 31 \\
    3.0 & 72 & 41 & 9 & 19 & 31 \\
    4.0 & 73 & 39 & 11 & 17 & 33 \\
    5.0 & 73 & 41 & 9 & 19 & 31 \\
    \end{tabular}
\end{table}

This table shows a slight increasing tendency in the accuracy, when the number
of queries grows, as well as a slight general shift of 'no' predictions to 'yes'
predictions. However, this increase is very minimal and can be disregarded. 

\section*{Discussion}

With an accuracy of about 73\% in the best-case scenarios, this method does not
prove very effective in actually predicting a user session. However, there are
some ideas that might be investigated in the future, to try and improve on this
method:

\begin{itemize}
    \item Use a non-symmetric non-triangular approach: follow-up queries might
        contain certain referencing words
    \item The experiments could be run using 4-shinglings. This, however,
        requires an advanced hardware setup. 
    \item Alternatively, instead of shinglings, use actual word occurrences as
        matrix keys. In this case, memory usage would be similar to the memory
        usage of 4-shinglings (1TB). 
    \item Investigate more sophisticated normalization / query testing
        techniques
    \item Try to obtain more data to train the matrix on. We only saw a slight
        increase in accuracy, but perhaps using massive amounts of data will
        eventually increase the performance of the session prediction. 
    \item Try to reduce the size of the matrix with matrix compression
        techniques. 
\end{itemize}

\section*{Conclusion}
This paper introduces a new way of recognizing queries belonging to the same
session using shinglings. Currently, a working implementation exists, and has an
accuracy of 73\% in the best cases. Some possible improvements have been
identified, and may be tested to see if this baseline accuracy can be improved.

\bibliographystyle{plain}
\bibliography{paper}

\end{document}
