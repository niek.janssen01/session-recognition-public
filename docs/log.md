# Log of interesting happenings
_**Niek Janssen, s4297091**_


  - log is kut
  - matrix drukken met sessies die (precies) 10.5 dag uit elkaar liggen
  - accenten verwijderen
  - stemming & stopping
  

## V1
Features:

  - Simple shingling matrix
  - Filling the matrix
  - Shingling counts in queries and in matrix
 
Log:

  - The progress bar does not work in Intellij IDE's, because the debugger 
    console is no TTY. Arch on windows, also in an intellij terminal, works 
    fine. A fallback progress output can be enabled in `const.ts`.
  - Apparently, some scrapers use bing to find (interesting) pages on websites. 
    This creates sessions of thousands of queries, hanging the program in a 
    *very* big loop. A limit on session sizes can be set in `const.ts`.
    - Implementing a limit on how many queries may exist in a session gives a 
      good performance
    - Throttling the number of query pairs in such sessions also gives good 
      performance, but it also almost *doubles* the number of counts in the 
      matrix. We may want to avoid this, because it will probably not all be
      quality data. 
    - Fun fact: a lot of those scraped websites are sex sites. 
  - Generator functions are very slow, especially when using them to compute a 
    dot product iterator. Performance was increased by 300% by using normal for
    loops. 

## V2

Features:

  - Create some graphs about the data, like shingling distribution, matrix 
    sparseness, matrix count distribution, session length distribution
    https://github.com/vmpowerio/chartjs-node
  - Implement caching
  - First tests from TREC data
  - Implement stemming and stopping
  - Simple dashboard
    
## Future versions

TODO: 

  - Is the process entirely symmetrical?
  - Try to predict session boundaries from TREC data (2014 is real user data)
  - Shingling lookup arrays 
  - Shingling grouping
  - Split matrix in dense and sparse part
  - Use space-efficient data structures in sparce matrix part
  - Sparce matrix with inverted file:
        shinglingA -> singlingB1, shinglingB2, ...  
