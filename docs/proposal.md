# Proposal: session recognition using query logs
_**Niek Janssen, s4297091**_

In the upcoming semester, I want to try to recognize sessions in query logs.
Eventually, the data we get from recognizing these sessions may be used to
recognize sessions in a real time system. 

## The main idea
To explain the main idea, we first look at its most simple, unoptimized form.
Below, we go into a little more detail about what needs to be done to make this
work and what variations we can try. 

Assume we have a matrix. On both axes, we have all different 3-shinglings.
Assume we have a cell with value $v$ on a position with shingling $s_1$ at the
left and $s_2$ at the top. This means there are $v$ pairs of queries $q_1$ and
$q_2$ for which the following holds:

  - query $q_1$ contains shingling $s_1$ and $q_2$ contains $s_2$
  - query $q_1$ and $q_2$ come from the same session, according to a very simple
      heuristic like: 'the time difference is smaller than 20 minutes'

To create the matrix, we simply loop over a query log and look at all pairs of
queries $q_1$ and $q_2$ issued by the same user within 20 minutes from each
other. Now, we increase for all pair of shinglings found in the queries the
corresponding value in the matrix by 1. 

To try and recognize sessions, we can consider two queries issued by the same
user and lookup their shingling values in the matrix. From this set of numbers
we can take the average, and check whether this average is above a certain
threshold. 

### Variations on the main idea
  - We may try matrices with different size shinglings, like 4, 5 or
      6-shinglings. However, a matrix containing all n-shinglings gets very big
      very quickly. More on this below. 
  - Instead of taking the average of the matrix values found combining
      shinglings from a pair of queries, we may also try to use other functions
      on sets of numbers like *mean*, *minimum* or a combination of those. 
  - We can try to use other 'simple heuristics' or timings other than the 20
      minute window to determine which pairs of queries can update the matrix. 

## Optimizations of the main idea
The most important optimization will be on the space needed to store the
shingling-matrix, especially when we want to consider 4, 5 or 6-shinglings. A
matrix of all possible pairs of 3-shinglings containing only lowercase letters
and the numbers 0-9 will already take a little over 8.5 gb when using 32 bit
integers. This number will *square* when we use 6-shinglings. To lower these
space requirements we can:

  - Use a triangular matrix, halving the space needed
  - Remove all shinglings that rarely occur from the matrix

To employ the second technique, we can use a translation array where we can
specify for each shingling whether it will occur in the matrix, and if so on
what position. In this way, we can cut away little used shinglings. Note that
the translation array for 6-shinglings is the same size as the matrix for all
3-shingling combinations. If those conversion arrays get big, we may stagger it
into multiple arrays, enabling us to cut whole groups of shinglings away in one
go. 

As an alternative to cutting away shinglings we may group them. However, if we
create a group of shinglings to put on an axis of the matrix, we should devide
the numbers generated in its column or row by the number of shinglings in the
group. Also note that  for some grouping heuristics, we cannot stagger
translation arrays for shingling grouping as easy as for shingling omitting, so
we cannot do this for shingling sizes bigger than 5. We can create such
shingling groups in many different ways:

  - grouping all shinglings (but if we do this, we may just as well choole
      smaller shinglings)
  - grouping all little-occurring shinglings in one group. We have to determine a
      threshold. This heuristic may also be used with staggered translation
      arrays. 
  - grouping shinglings in groups so that: 
    - two shinglings in the same group have about the same number of occurrences
        in the dataset
    - the total number of occurrences of the shinglings in a group should be
        about the same for each group

To be able to implement grouping or omitting of shinglings, we have to make a
pass over the dataset, count all occurring shinglings and make a histogram out
of it. This will give us some insight about what data we are dealing with
exactly and how we should structure such a translation array. 

## Available data
To fit the matrix, we can use the Bing dataset from a few years ago. It contains
15 million queries. For each query, a session id has already been stored. As I
do not know how those session id's are created by Bing, I will assume that they
only identify a user. I will make a pass of the dataset extracting some info
about the session id's. 

To test our generated matrix, we can use the TREC dataset.
The TREC dataset has the advantage that sessions are already marked, so we can
test how well this solution works. As for now, I am unsure whether these
datasets (especially the training dataset) is big enough. When coming up with
this idea, I had Google-like datasets in mind. 

## Implementation
To create the implementations of the ideas described above, I will use
Typescript. Typescript is a variation on Javascript I already used extensively,
so I am very familiar with it. It runs considerably faster than python and it
supports arrays with specifically sized integers (either signed or unsigned). 
