/********************************
 * Algorithm parameter settings *
 ********************************/

// Array of tasks to be executed.
export const TASKS = ['dashboard'];

// Shingling size. Make sure you have enough RAM before increasing
export const SHINGLING_SIZE = 3;

// Query offset. Add shinglings to the matrix if there is less time than this
// between their queries.
export const QUERY_OFFSET_MS = 5 * 60 * 1000;

// Query files
export const QUERY_FILES = ['data/queries.txt'];

// TREC files
export const TREC_FILES = ['data/trec14.xml'];

// Avoid spending a lot of time on scrapers
export const MAX_SESSION_LENGTH = 100;

// Use on non-TTY consoles where the progress bar does not work
export const PROGRESS_BAR_FALLBACK = false;

// Throttle progress bar ticks
export const PROGRESS_BAR_TICK_LOOPCOUNT = 100 * 1000;

// Number of datapoints used to create a chart, generally on the x-axis
export const CHART_DATAPOINTS = 30;

// Number of query pairs from the TREC dataset to test
export const TREC_TEST_DATAPOINTS = 10 * 1000;

// Implementation of matrix. Can be:
// - simpleMatrix: direct access matrix (will be large!)
// - hashedMatrix: compacter version using hashes (will still be large!)
export const MATRIX_IMPLEMENTATION = 'matrix';

/***********************************
 * Fixed constants, DO NOT CHANGE! *
 ***********************************/
             // The number of characters that can be represented in a shingling. Currently
             // a-z and 0-1
export const SHINGLING_CHARACTER_NUMBER = 36;

// The number of bytes used for each matrix cell. Currently we use 32-bit
// unsigned integers, so the byte size is 4
export const MATRIX_CELL_SIZE = 4;

// The total dimension of the matrix and/or queryCounts
export const MATRIX_DIMENSION = Math.pow(SHINGLING_CHARACTER_NUMBER, SHINGLING_SIZE);

// The directory where the cache is stored
export const CACHE_DIR = 'data/cache';

// The directory where charts and output data is stored
export const OUTPUT_DIR = 'out';

// Due to V8 limitations, the max length of a buffer is currently 2GiB - 1 byte.
// We split all cache files into multiple files of max 1GB.
export const BUFFER_MAX_LENGTH = 2 ** 29;
