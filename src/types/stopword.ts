declare module 'stopword' {
    export function removeStopwords (oldString: string[]): string[];
}
