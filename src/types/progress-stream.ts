declare module 'progress-stream' {
    import ReadStream = NodeJS.ReadStream;

    interface ProgressArgumentObject {
        time: number,
        length?: number
    }

    interface ProgressCallbackObject {
        percentage: number,
        transferred: number,
        length: number,
        remaining: number,
        eta: number,
        runtime: number
        delta: number,
        speed: number
    }

    type ProgressCallback = (progress: ProgressCallbackObject) => void

    function progress (args: ProgressArgumentObject, callback?: ProgressCallback): ReadStream

    export = progress
}
