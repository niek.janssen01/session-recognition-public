import {MapStream} from 'event-stream';

// The @types for event-stream are for version 3.3.2. This file declares the
// missing types so we can also use those functions.
declare module 'event-stream' {
    export function filterSync (syncFunction: Function): MapStream;
}
