import {QUERY_FILES}                                   from './constant';
import {matrixCacheHandler, matrixTask, normalizeTask} from './internals/simplematrix';
import {dashboardTask}                                 from './output/dashboard';
import {statTasks}                                     from './output/stats';
import {parserObjectTask, parseTask}                   from './parsers/queryfileparser';
import {parseTRECTask}                                 from './parsers/trec-parser';
import {trecDataTest}                                  from './tests/trec-tester';
import {invalidateCache}                               from './util/cache';
import {NoPrintableResult}                             from './util/resulttransformers';
import {DependencyResultMap, start, task}              from './util/task';
import {cutoffAndConfusion}                            from './tests/cutoffAndConfusion';
import ProgressBar = require('progress');

Error.stackTraceLimit = Infinity;

async function main() {
    await invalidateCache();
    matrixTask();
    normalizeTask();
    parserObjectTask();

    const parserDeps = QUERY_FILES.map((file: string) => parseTask(file));
    parserDeps.push('matrix');
    task({
        name: 'parse',
        title: 'Parsed all required files: ',
        deps: parserDeps,
        totalTicks: 1,
        resultTransformer: NoPrintableResult,
        cache: matrixCacheHandler,
        compute: async (bar: ProgressBar, {matrix}: DependencyResultMap) => matrix,
    });

    statTasks();

    parseTRECTask();
    trecDataTest();
    cutoffAndConfusion();

    dashboardTask();

    //printDependencies();
    await start();
}

main();
