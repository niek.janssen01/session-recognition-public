export type QueryTest = (vals: number[], testParameters: any) => number
export  type QueryTestNames = 'average' | 'median' | 'threshold'

export const queryTests: { [key: string]: QueryTest } = {};

queryTests.average = (vals: number[]) => {
    let avg = 0;
    for (const val of vals)
        avg += val;
    return avg / vals.length;
};

queryTests.median = (vals: number[]) => {
    vals.sort((a: number, b: number) => a - b);
    return (vals[Math.ceil((vals.length - 1) / 2)] +
            vals[Math.floor((vals.length - 1) / 2)]) / 2;
};

queryTests.tuueshold = (vals: number[], {threshold: th}: any) => {
    return vals.filter((val: number) => val >= th).length / vals.length;
};
