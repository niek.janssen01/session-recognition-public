import {SHINGLING_SIZE}                        from '../constant';
import {TriangularShinglingMatrix}             from '../internals/simplematrix';
import {extractShinglings}                     from '../internals/shinglings';
import {QueryTest, QueryTestNames, queryTests} from './querytests';

export class QueryTester {
    queryTest: QueryTest;
    valueDistribution = [
        Array(1800)
            .fill(0),
        Array(1800)
            .fill(0),
    ];

    constructor (private shinglingMatrix: TriangularShinglingMatrix,
                 test: QueryTestNames,
                 private testArgs?: any) {
        this.queryTest = queryTests[test];
    }

    test (q1: string, q2: string, expected: boolean): number {
        const q1ss = extractShinglings(q1, SHINGLING_SIZE);
        const q2ss = extractShinglings(q2, SHINGLING_SIZE);

        const values = [];

        for (const q1s of q1ss) {
            for (const q2s of q2ss)
                values.push(this.shinglingMatrix.read(q1s, q2s));
        }

        const result = this.queryTest(values, this.testArgs);
        this.valueDistribution[expected ? 1 : 0][Math.floor(result)]++;
        return result;
    }
}
