import {task}                                 from '../util/task';
import {identity}                             from 'rxjs';
import * as ProgressBar                       from 'progress';
import {plus}                                 from '../util/util';
import {jsonCacheHandler, numberCacheHandler} from '../util/cache';

export function cutoffAndConfusion() {
    task({
        name: `cutoffValue`,
        title: `Computing cutoff value`,
        deps: ['testWithTRECData'],
        totalTicks: 1,
        resultTransformer: identity,
        cache: numberCacheHandler,
        compute: async (bar: ProgressBar,
                        {testWithTRECData}: { testWithTRECData: { average: { true: number[], false: number[] } } }) => {
            let trueCorrect      = testWithTRECData.average.true.reduce(plus);
            let falseCorrect     = 0;
            let mostCorrectKey   = 0;
            let mostCorrectValue = 0;

            for (let i = 0; i < testWithTRECData.average.true.length; i++) {
                trueCorrect -= testWithTRECData.average.true[i];
                falseCorrect += testWithTRECData.average.false[i];
                if (trueCorrect + falseCorrect > mostCorrectValue) {
                    mostCorrectValue = trueCorrect + falseCorrect;
                    mostCorrectKey   = i;
                }
            }
            return mostCorrectKey;
        },
    });

    task({
        name: `cutoffConfusionMatrixRaw`,
        title: `Cutoff value confusion matrix (absolute)`,
        deps: ['testWithTRECData', 'cutoffValue'],
        totalTicks: 1,
        resultTransformer: identity,
        cache: jsonCacheHandler('cutoffConfusionMatrixRaw'),
        compute: async (bar: ProgressBar,
                        {testWithTRECData, cutoffValue}:
                            { testWithTRECData: { average: { true: number[], false: number[] } }, cutoffValue: number }) => {
            return [
                testWithTRECData.average.false.slice(0, cutoffValue).reduce(plus, 0),
                testWithTRECData.average.false.slice(cutoffValue).reduce(plus, 0),
                testWithTRECData.average.true.slice(0, cutoffValue).reduce(plus, 0),
                testWithTRECData.average.true.slice(cutoffValue).reduce(plus, 0),
            ];
        },
    });

    task({
        name: `cutoffConfusionMatrix`,
        title: `Cutoff value confusion matrix (display)`,
        deps: ['cutoffConfusionMatrixRaw'],
        totalTicks: 1,
        resultTransformer: identity,
        cache: jsonCacheHandler('cutoffConfusionMatrix'),
        compute: async (bar: ProgressBar,
                        {cutoffConfusionMatrixRaw}: { cutoffConfusionMatrixRaw: number[] }) => {
            const total = cutoffConfusionMatrixRaw.reduce(plus, 0);
            return cutoffConfusionMatrixRaw.map(num => `${Math.round(num * 100 / total)}%`);
        },
    });

    task({
        name: 'accuracy',
        title: 'Total accuracy',
        deps: ['cutoffConfusionMatrixRaw'],
        totalTicks: 1,
        resultTransformer: identity,
        cache: numberCacheHandler,
        compute: async (bar: ProgressBar, {cutoffConfusionMatrixRaw}: { cutoffConfusionMatrixRaw: number[] }) => {
            return Math.round((cutoffConfusionMatrixRaw[0] + cutoffConfusionMatrixRaw[3]) * 100 / cutoffConfusionMatrixRaw.reduce(plus));
        },
    });
}
