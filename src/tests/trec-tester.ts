import {TREC_TEST_DATAPOINTS}      from '../constant';
import {TriangularShinglingMatrix} from '../internals/simplematrix';
import {TRECData, TRECSession}     from '../parsers/trec-parser';
import {jsonCacheHandler}          from '../util/cache';
import {NoPrintableResult}         from '../util/resulttransformers';
import {DependencyResultMap, task} from '../util/task';
import {QueryTester}               from './querytester';
import ProgressBar = require('progress');

export function trecDataTest() {
    task({
        name: `testWithTRECData`,
        title: `Testing dataset with TREC data`,
        deps: ['normalize', 'parseTREC'],
        totalTicks: TREC_TEST_DATAPOINTS,
        resultTransformer: NoPrintableResult,
        cache: jsonCacheHandler('testWithTRECData'),
        compute: async (bar: ProgressBar,
                        {normalize: matrixOBj, parseTREC: trecData}: DependencyResultMap) => {
            const [averageTester, medianTester] = testWithTRECData(bar, trecData, matrixOBj);

            return {
                average: {
                    true: averageTester.valueDistribution[1],
                    false: averageTester.valueDistribution[0],
                },
                median: {
                    true: medianTester.valueDistribution[1],
                    false: medianTester.valueDistribution[0],
                },
            };
        },
    });
}

function testWithTRECData(bar: ProgressBar, trecData: TRECData,
                          matrix: TriangularShinglingMatrix): [QueryTester, QueryTester] {
    const averageTester = new QueryTester(matrix, 'average');
    const medianTester  = new QueryTester(matrix, 'median');
    for (const [q1, q2] of TRECSameSessionQueryPairs(trecData)) {
        averageTester.test(q1, q2, true);
        medianTester.test(q1, q2, true);
        bar.tick();
    }
    for (const [q1, q2] of TRECOtherSessionQueryPairs(trecData)) {
        averageTester.test(q1, q2, false);
        medianTester.test(q1, q2, false);
        bar.tick();
    }

    return [averageTester, medianTester];
}

function createTRECWeightedAverageArray(trecData: TRECData): TRECData {
    const weightedSessionArray: TRECSession[] = [];
    for (const session of trecData) {
        for (let i = 0; i < session.length; i++)
            weightedSessionArray.push(session);
    }
    return weightedSessionArray;
}

function* TRECSameSessionQueryPairs(trecData: TRECData): IterableIterator<[string, string]> {
    for (let i = 0; i < TREC_TEST_DATAPOINTS / 2; i++) {
        const session = getRandomElement(trecData);
        yield getRandomElementPair(session, false);
    }
}

function* TRECOtherSessionQueryPairs(trecData: TRECData): IterableIterator<[string, string]> {
    for (let i = 0; i < TREC_TEST_DATAPOINTS / 2; i++)
        yield  getRandomElementPair(trecData, false)
            .map((session: TRECSession) => getRandomElement(session)) as [string, string];
}

function getRandomElement<T>(array: T[]): T {
    return array[Math.floor(Math.random() * array.length)];
}

function getRandomElementPair<T>(array: T[], canBeSame: boolean): [T, T] {
    const e1 = getRandomElement(array);
    const e2 = getRandomElement(array);
    if (e1 !== e2 || canBeSame)
        return [e1, e2];
    return getRandomElementPair(array, canBeSame);
}
