import {jsonCacheHandler}          from '../util/cache';
import {NoPrintableResult}         from '../util/resulttransformers';
import {DependencyResultMap, task} from '../util/task';
import ProgressBar = require('progress');

export function dashboardTask() {
    task({
        name: 'dashboard',
        title: 'Creating dashboard: ',
        deps: ['queryShinglings', 'matrixShinglings', 'density', 'distributedDensity', 'testWithTRECData', 'cutoffValue', 'cutoffConfusionMatrix', 'accuracy'],
        totalTicks: 1,
        resultTransformer: NoPrintableResult,

        cache: jsonCacheHandler('dashboard', 'output', true),

        compute: async (bar: ProgressBar,
                        {
                            queryShinglings,
                            matrixShinglings,
                            density,
                            distributedDensity,
                            testWithTRECData,
                            cutoffValue,
                            cutoffConfusionMatrix,
                            accuracy,
                        }: DependencyResultMap) => {
            return {
                queryShinglings,
                matrixShinglings,
                density,
                distributedDensity,
                testWithTRECData,
                cutoffValue,
                cutoffConfusionMatrix,
                accuracy,
            };
        },
    });
}
