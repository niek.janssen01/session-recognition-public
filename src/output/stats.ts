import {identity}                                                        from 'rxjs';
import {CHART_DATAPOINTS, MATRIX_DIMENSION, PROGRESS_BAR_TICK_LOOPCOUNT} from '../constant';
import {jsonCacheHandler, numberCacheHandler}                            from '../util/cache';
import {NoPrintableResult, percentage}                                   from '../util/resulttransformers';
import {DependencyResultMap, task}                                       from '../util/task';
import {matrixSize}                                                      from '../internals/simplematrix';
import ProgressBar = require('progress');

export function statTasks() {
    densityStatTask();
    distributedDensityStatTask();
    queryShinglingStatTask();
    matrixShinglingStatTask();
}

function densityStatTask() {
    task({
        name: 'density',
        title: 'Shingling matrix density: ',
        deps: ['normalize'],
        totalTicks: matrixSize() / PROGRESS_BAR_TICK_LOOPCOUNT,
        resultTransformer: percentage,

        cache: numberCacheHandler,

        compute: async (bar: ProgressBar, {normalize: {matrixBuffer}}: DependencyResultMap) => {
            const tickSize = 100 * 1000;
            let tickCount  = 0;
            let nonZero    = 0;
            for (let i = 0; i < matrixBuffer.length; i++) {
                if (matrixBuffer[i] !== 0)
                    nonZero++;
                if (tickCount % tickSize === 0)
                    bar.tick();
                tickCount++;
            }
            return nonZero * 100 / matrixBuffer.length;
        },
    });
}

function distributedDensityStatTask() {
    task({
        name: 'distributedDensity',
        title: 'Distributed matrix density:',
        deps: ['normalize'],
        totalTicks: MATRIX_DIMENSION,
        resultTransformer: NoPrintableResult,

        cache: jsonCacheHandler('distributedDensity'),
        compute: async (bar: ProgressBar, {normalize: {matrix}}: DependencyResultMap) => {

            let distributedDensity = [];
            for (let s = 0; s < MATRIX_DIMENSION; s++) {
                let count = 0;
                for (let i = 0; i < s; i++)
                    count += matrix[s][i] === 0 ? 0 : 1;
                for (let i = s; i < MATRIX_DIMENSION; i++)
                    count += matrix[i][s] === 0 ? 0 : 1;
                distributedDensity.push(count);
                bar.tick();
            }
            distributedDensity = distributedDensity.sort((a: number, b: number) => a - b)
                                                   .reverse();

            const condensedDistributedDensity = [];
            const chunkSize                   = Math.floor(distributedDensity.length / CHART_DATAPOINTS);
            for (let i = 0; i < CHART_DATAPOINTS; i++) {
                let count = 0;
                for (let j = 0; j < chunkSize; j++)
                    count += distributedDensity[chunkSize * i + j];
                condensedDistributedDensity.push(count / MATRIX_DIMENSION / chunkSize * 100);
            }

            return {densityArray: condensedDistributedDensity};
        },
    });
}

function queryShinglingStatTask() {
    task({
        name: 'queryShinglings',
        title: 'Shinglings in queries: ',
        deps: ['normalize'],
        totalTicks: 1,
        resultTransformer: identity,

        cache: numberCacheHandler,

        compute: (bar: ProgressBar, {normalize: {queryCounts}}: DependencyResultMap) => {
            bar.tick();
            return queryCounts.reduce((a: number, b: number) => a + b);
        },
    });
}

function matrixShinglingStatTask() {
    task({
        name: 'matrixShinglings',
        title: 'Shingling pairs in matrix: ',
        deps: ['parse'],
        totalTicks: matrixSize() / PROGRESS_BAR_TICK_LOOPCOUNT,
        resultTransformer: identity,

        cache: numberCacheHandler,

        compute: async (bar: ProgressBar, {parse: {matrixBuffer}}: DependencyResultMap) => {
            const tickSize = 100 * 1000;
            let tickCount  = 0;
            let count      = 0;
            for (let i = 0; i < matrixBuffer.length; i++) {
                count += matrixBuffer[i];
                if (tickCount % tickSize === 0)
                    bar.tick();
                tickCount++;
            }
            return count;
        },
    });
}

