import {readFileSync}              from 'fs';
import {parseString}               from 'xml2js';
import {TREC_FILES}                from '../constant';
import {NoPrintableResult}         from '../util/resulttransformers';
import {DependencyResultMap, task} from '../util/task';
import ProgressBar = require('progress');

export type TRECData = TRECSession[];
export type TRECSession = string[];

export function parseTRECTask () {
    task({
             name             : 'parseTREC',
             title            : 'Parsing TREC dataset',
             deps             : [],
             totalTicks       : 1,
             resultTransformer: NoPrintableResult,
             compute          : async (bar: ProgressBar, {parse: matrixObj}: DependencyResultMap) => {
                 const data = await getTrecData();
                 bar.tick();
                 return data;
             },
         });
}


async function getTrecData (): Promise<TRECData> {
    const result: TRECData = [];
    for (const file of TREC_FILES) {
        const xmlString = readFileSync(file);
        const jsonObj   = await new Promise((resolve) =>
                                                parseString(xmlString, (err, data) => resolve(data)));
        extractQuerySessions(jsonObj, result);
    }
    return result;
}

function extractQuerySessions (json: any, set: TRECData) {
    for (const session of json.sessiontrack2014.session) {
        const sessionQueries: TRECSession = [];
        for (const interaction of session.interaction)
            if (!sessionQueries.includes(interaction.query[0]))
                sessionQueries.push(interaction.query[0]);
        if (sessionQueries.length > 1)
            set.push(sessionQueries);
    }
}
