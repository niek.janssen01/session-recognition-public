import {filterSync, mapSync, split} from 'event-stream';
import * as fs                      from 'fs';
import {createReadStream, statSync} from 'fs';
import {
    MATRIX_IMPLEMENTATION,
    MAX_SESSION_LENGTH,
    PROGRESS_BAR_FALLBACK,
    QUERY_OFFSET_MS,
    SHINGLING_SIZE,
}                                   from '../constant';
import {extractShinglings}          from '../internals/shinglings';
import {TriangularShinglingMatrix}  from '../internals/simplematrix';
import '../types/event-stream';
import {Pool}                       from '../util/pool';
import {NoPrintableResult}          from '../util/resulttransformers';
import {
    DependencyResultMap,
    task,
}                                   from '../util/task';
import ProgressBar = require('progress');
import progress = require('progress-stream');

// QueryData object
class QueryData {
    public timestamp: number    = 0;
    public query: string        = '';
    public session: string      = '';
    public shinglings: number[] = [];

    reset (): QueryData {
        this.shinglings.length = 0;
        return this;
    }
}

export class BingFileParser {
    // Map storing sessions -> queryCounts of [timestamp, query, sessionid]
    sessionMap = new Map<string, QueryData[]>();

    // Queue storing past queries, to be able to process a Bing session when:
    //   - The last query of the Bing session is encountered
    //   - A 30 minute gap between two queries in a Bing sessions exists
    sessionQueue: QueryData[] = [];

    // Object pool to relieve the garbage collector somewhat
    qdPool = new Pool<QueryData>(() => new QueryData(), (qd: QueryData) => qd.reset());

    constructor (
        public shinglingMatrix: TriangularShinglingMatrix,
        public shinglingArray: Uint32Array,
    ) {
    }


    parseBingFile (filename: string, progressBar: ProgressBar): Promise<TriangularShinglingMatrix> {
        // Boolean used to filter the first element of the queryCounts.
        let header             = true;
        const progressListener = progress({
                                              time  : 1000,
                                              length: fs.statSync(filename).size,
                                          }, (progress) => {
            progressBar.tick(progress.delta);
            if (PROGRESS_BAR_FALLBACK)
                console.log(progress.percentage);
        });

        return new Promise((resolve: (matrix: TriangularShinglingMatrix) => void, reject: (reason: any) => void) => {
            const stream = createReadStream(filename)
                .pipe(progressListener)
                .pipe(split())
                .pipe(filterSync((line: string) => {
                    // Filter the first row (containing the column headers) and
                    // trailing empty lines
                    if (!header)
                        return line !== '';
                    header = false;
                    return false;
                }))
                .pipe(mapSync((line: string) => {
                    const qdt: QueryData = this.parseLine(line);
                    this.groupQueriesIntoSessions(qdt);
                    this.processFilledQueryGroups(qdt.timestamp);
                }))
                .on('close', () => {
                    this.handleResidualSessions();
                    resolve(this.shinglingMatrix);
                })
                .on('error', reject);
        });
    }

    parseLine (line: string): QueryData {
        // Parse log line into a QueryData object containing the UTC timestamp, query and sessionId
        const [dateTime, query, _1, sessionId, _2] = line.split('\t');

        const [date, time]           = dateTime.split(' ');
        const [year, month, day]     = date.split('-')
                                           .map((n: string) => parseInt(n));
        const [hour, minute, second] = time.split(':')
                                           .map((n: string) => parseInt(n));
        const timestamp              = Date.UTC(year, month, day, hour, minute, second);

        const queryData     = this.qdPool.getNew();
        queryData.timestamp = timestamp;
        queryData.query     = query;
        queryData.session   = sessionId;
        return queryData;
    }

    groupQueriesIntoSessions (qdt: QueryData) {
        // Group the query data tuples by their Bing session id.
        // Make sure a Bing session group exists for the current session and
        // add the current query to it
        if (!this.sessionMap.has(qdt.session))
            this.sessionMap.set(qdt.session, []);
        this.sessionMap.get(qdt.session)!.push(qdt);
        this.sessionQueue.push(qdt);
    }

    processFilledQueryGroups (timestamp: number) {
        // Dequeue previously parsed queries, as long as they are more than
        // QUERY_OFFSET_MS earlier than the current query
        while (true) {
            const queueSession = this.sessionQueue[0];
            if (queueSession === undefined || queueSession.timestamp + QUERY_OFFSET_MS > timestamp)
                break;
            this.sessionQueue.shift();

            // Retrieve the last encountered query of the Bing session of this query
            const sessionQueries = this.sessionMap.get(queueSession.session)!;
            if (sessionQueries === undefined)
                continue;
            const lastQuery = sessionQueries[sessionQueries.length - 1];

            // If this query is less than 30 minutes ago, do nothing
            if (lastQuery.timestamp + QUERY_OFFSET_MS > timestamp)
                continue;

            // If this query is more than 30 minutes ago, conclude and handle
            // this Bing session
            this.handleSession(sessionQueries);
            this.deleteSessionFromMap(queueSession.session);
        }
    }

    handleResidualSessions () {
        // Process all sessions that were not closed by the end of the file
        for (const [_, qds] of this.sessionMap)
            this.handleSession(qds);
    }

    deleteSessionFromMap (session: string) {
        // Delete a handled session from the session map
        const queries = this.sessionMap.get(session)!;
        this.sessionMap.delete(session);
        // Retire all used data structures
        for (const query of queries)
            this.qdPool.retire(query);
    }

    handleSession (qds: QueryData[]) {
        // Extract the shinglings out of the queries, and map them
        qds.forEach((qd: QueryData) => qd.shinglings = extractShinglings(
            qd.query, SHINGLING_SIZE, this.shinglingArray));
        this.mapQueryShinglingObjects(qds);
    }

    private mapQueryShinglingObjects (qds: QueryData[]) {
        // Skip long sessions, because they are probably from scrapers
        if (qds.length > MAX_SESSION_LENGTH)
            return;

        // Loop over pairs of queries with times in range of each other
        for (let ia = 0; ia < qds.length - 1; ia++) {
            const qa = qds[ia];
            for (let ib = ia + 1; ib < qds.length && qds[ib].timestamp - qa.timestamp < QUERY_OFFSET_MS; ib++) {
                const qb = qds[ib];
                // For each of those pairs of queries, count all their pairs of
                // shinglings in the matrix
                for (const sa of qa.shinglings)
                    for (const sb of qb.shinglings)
                        this.shinglingMatrix.count(sa, sb);
            }
        }
    }
}

export function parserObjectTask () {
    task({
             name             : 'parser',
             title            : 'Creating parser: ',
             deps             : [MATRIX_IMPLEMENTATION],
             totalTicks       : 1,
             resultTransformer: NoPrintableResult,
             compute          : async (bar: ProgressBar, {matrix}: DependencyResultMap) => {
                 bar.tick();
                 return new BingFileParser(matrix, matrix.queryCounts);
             },
         });
}

export function parseTask (filename: string): string {
    const fileSize = statSync(filename).size;
    const taskName = `parse-${filename}`;
    task({
             name             : taskName,
             title            : `Processing ${filename}: `,
             deps             : ['parser'],
             totalTicks       : fileSize,
             resultTransformer: NoPrintableResult,
             compute          : (bar: ProgressBar, {parser}: DependencyResultMap) => {
                 return parser.parseBingFile(filename, bar);
             },
         });
    return taskName;
}
