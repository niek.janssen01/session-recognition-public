import {writeFile}                                                       from 'fs';
import {noop}                                                            from 'rxjs';
import {MATRIX_CELL_SIZE, MATRIX_DIMENSION, PROGRESS_BAR_TICK_LOOPCOUNT} from '../constant';
import {arrayBufferCacheHandler, jsonCacheHandler}                       from '../util/cache';
import {NoPrintableResult}                                               from '../util/resulttransformers';
import {DependencyResultMap, task, TaskCacheDescriptor}                  from '../util/task';
import {SharedMatrixObject}                                              from './sharedmatrixobject';
import ProgressBar = require('progress');

export class TriangularShinglingMatrix extends SharedMatrixObject {
    matrixBuffer: Uint32Array = new Uint32Array(0);
    lookupArray: number[]     = [];
    lookupArrayCount          = 0;
    matrix: Uint32Array[]     = [];

    constructor(bar: ProgressBar)
    constructor(matrixBuffer: ArrayBuffer | SharedArrayBuffer, queryCountBuffer: ArrayBuffer | SharedArrayBuffer,
                queryPairBuffer?: ArrayBuffer | SharedArrayBuffer, lookupInfo?: ArrayCountJson)
    constructor(barOrMatrix: ProgressBar | ArrayBuffer | SharedArrayBuffer,
                queryCountBuffer?: ArrayBuffer | SharedArrayBuffer,
                queryPairBuffer?: ArrayBuffer | SharedArrayBuffer) {
        super();
        this.constructMatrix(barOrMatrix instanceof ArrayBuffer ? barOrMatrix : undefined);
        if (barOrMatrix instanceof ProgressBar)
            this.matrixInitZero(barOrMatrix);

        this.createShinglingArrays(queryCountBuffer, queryPairBuffer);
    }

    count(a: number, b: number) {
        if (a < b)
            this.matrix[b][a]++;
        else
            this.matrix[a][b]++;
        this.pairCounts[a]++;
        this.pairCounts[b]++;
    }

    read(a: number, b: number): number {
        if (a < b)
            return this.matrix[b][a];
        else
            return this.matrix[a][b];
    }

    private matrixInitZero(bar: ProgressBar) {
        let x = 0;
        for (let i = 0; i < MATRIX_DIMENSION; i++) {
            for (let j = 0; j <= i; j++) {
                this.matrix[i][j] = 0;
                if (x % PROGRESS_BAR_TICK_LOOPCOUNT === 0)
                    bar.tick();
                x++;
            }
        }
    }


    private constructMatrix(matrixBuffer?: ArrayBuffer | SharedArrayBuffer) {
        const size         = matrixSize();
        const buffer       = matrixBuffer || new ArrayBuffer(size * MATRIX_CELL_SIZE);
        this.matrixBuffer  = new Uint32Array(buffer);
        this.matrix        = [];
        this.matrix.length = MATRIX_DIMENSION;
        let offset         = 0;
        let row            = 0;
        while (row < MATRIX_DIMENSION) {
            const length     = row + 1;
            this.matrix[row] = new Uint32Array(buffer, offset * MATRIX_CELL_SIZE, length);
            offset += length;
            row += 1;
        }
    }

    private createShinglingArrays(queryCountBuffer?: ArrayBuffer | SharedArrayBuffer,
                                  pairCountBuffer?: ArrayBuffer | SharedArrayBuffer,
                                  lookupBuffer?: ArrayCountJson) {
        queryCountBuffer = queryCountBuffer || new ArrayBuffer(MATRIX_DIMENSION * MATRIX_CELL_SIZE);
        this.queryCounts = new Uint32Array(queryCountBuffer, 0, MATRIX_DIMENSION);
        pairCountBuffer  = pairCountBuffer || new ArrayBuffer(MATRIX_DIMENSION * MATRIX_CELL_SIZE);
        this.pairCounts  = new Uint32Array(pairCountBuffer, 0, MATRIX_DIMENSION);
        if (lookupBuffer) {
            this.lookupArray      = lookupBuffer.array;
            this.lookupArrayCount = lookupBuffer.count;
        }
    }
}

export function matrixSize(): number {
    return MATRIX_DIMENSION % 1 == 0 ?
           MATRIX_DIMENSION * (MATRIX_DIMENSION + 1) / 2 :
           (MATRIX_DIMENSION - 1) * (MATRIX_DIMENSION - 1) / 2 + MATRIX_DIMENSION;
}

export function matrixTask() {
    task({
        name: 'matrix',
        title: 'Create matrix datastructure: ',
        deps: [],
        totalTicks: matrixSize() / PROGRESS_BAR_TICK_LOOPCOUNT,
        resultTransformer: NoPrintableResult,
        compute: async (bar: ProgressBar) => new TriangularShinglingMatrix(bar),
    });
}

interface ArrayCountJson {
    array: number[];
    count: number;
}

export const matrixCacheHandler: TaskCacheDescriptor<TriangularShinglingMatrix> = {
    load: async (filename: string) => {
        const matrixPromise      = await arrayBufferCacheHandler.load(filename + '.matrix');
        const queryCountPromise  = await arrayBufferCacheHandler.load(filename + '.queryCounts');
        const pairCountPromise   = await arrayBufferCacheHandler.load(filename + '.pairCounts');
        const lookupArrayPromise = await jsonCacheHandler(filename).load(filename + '.lookupArray');
        if (matrixPromise === null || queryCountPromise === null || pairCountPromise === null || lookupArrayPromise === null)
            return null;
        return new TriangularShinglingMatrix(matrixPromise, queryCountPromise, pairCountPromise, lookupArrayPromise);
    },
    save: async (filename: string, matrix: TriangularShinglingMatrix) => {
        await arrayBufferCacheHandler.save(filename + '.matrix', matrix.matrixBuffer.buffer);
        await arrayBufferCacheHandler.save(filename + '.queryCounts', matrix.queryCounts.buffer);
        await arrayBufferCacheHandler.save(filename + '.pairCounts', matrix.pairCounts.buffer);
        await jsonCacheHandler(filename).save(filename + '.lookupArray',
            {array: matrix.lookupArray, count: matrix.lookupArrayCount});
        writeFile(filename, 'exists', noop);
    },
};

export function normalizeTask() {
    task({
        name: 'normalize',
        title: 'Normalizing query matrix: ',
        deps: ['parse', 'matrixShinglings'],
        totalTicks: MATRIX_DIMENSION,
        resultTransformer: NoPrintableResult,
        cache: matrixCacheHandler,
        compute: async (bar: ProgressBar, {parse: matrixObj}: DependencyResultMap) => {
            const modifier = 1000;
            for (let s = 0; s < MATRIX_DIMENSION; s++) {
                for (let i = 0; i <= s; i++) {
                    if (matrixObj.pairCounts[s] === 0)
                        continue;
                    const divisor          = Math.sqrt(matrixObj.pairCounts[s] * matrixObj.pairCounts[i]);
                    const normalized       = matrixObj.matrix[s][i] * modifier * MATRIX_DIMENSION / divisor;
                    const ln               = 100 * Math.floor(Math.max(0, Math.log(normalized)));
                    matrixObj.matrix[s][i] = ln;
                }
                bar.tick();
            }
            return matrixObj;
        },
    });
}
