export abstract class SharedMatrixObject {
    queryCounts: Uint32Array = new Uint32Array(0);
    pairCounts: Uint32Array = new Uint32Array(0);

    protected constructor() {
    }

    abstract count(a: number, b: number): void;

    abstract read(a: number, b: number): number;
}
