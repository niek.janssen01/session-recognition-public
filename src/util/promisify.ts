import {mkdir, readFile, writeFile} from 'fs';
import {emptyDir}                   from 'fs-extra';
import {promisify}                  from 'util';

export const pWriteFile = promisify(writeFile);
export const pReadFile  = promisify(readFile);
export const pMkDir     = promisify(mkdir);
export const pEmptyDir  = promisify(emptyDir);
