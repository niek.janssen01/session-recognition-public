export class Pool<T> {
    private pool: T[] = [];

    constructor(private create: () => T, private reset: (t: T) => T) {
    }

    getNew(): T {
        const obj = this.pool.length === 0 ?
            this.create() :
            this.pool.pop()!;
        return this.reset(obj);
    }

    retire(object: T) {
        this.pool.push(object);
    }
}
