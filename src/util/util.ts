/**
 * Base64URL encode a string
 *
 * @param unencoded the string to be encoded
 */
export function b64encode(unencoded: string): string {
    return Buffer.from(unencoded).toString('base64url');
};

/**
 * Fix the width of a string. If the string is too short, pad it with spaces. If it is too long, truncate it and replace
 * the last three characters with ...
 *
 * @param str The string to be extended/truncated
 * @param width The fixed width
 */
export function fixedStringWidth(str: string, width: number) {
    if (str.length <= width)
        return str.padEnd(width);
    return str.substr(0, width - 3) + '...';
}

/**
 * Transform a buffer into an ArrayBuffer or SharedArrayBuffer
 *
 * @param buffer
 */
export function b2ab(buffer: Buffer): ArrayBuffer | SharedArrayBuffer {
    return buffer.buffer.slice(buffer.byteOffset, buffer.byteOffset + buffer.byteLength);
}

/**
 * Round a number to a number of decimals
 *
 * @param n The number to be rounded
 * @param decimals The number of decimals to be rounded to
 */
export function round(n: number, decimals: number) {
    return Math.round(n * (10 ** decimals)) / 10 ** decimals;
}

/**
 * Plus function to be used with reduce
 *
 * @param a
 * @param b
 */
export function plus(a: number, b: number): number {
    return a + b;
}