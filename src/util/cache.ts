import {createReadStream, existsSync, readFileSync} from 'fs';
import {
    BUFFER_MAX_LENGTH,
    CACHE_DIR,
    MATRIX_CELL_SIZE,
    MAX_SESSION_LENGTH,
    OUTPUT_DIR,
    QUERY_FILES,
    QUERY_OFFSET_MS,
    SHINGLING_SIZE,
}                                                   from '../constant';
import {pEmptyDir, pMkDir, pReadFile, pWriteFile}   from './promisify';
import {TaskCacheDescriptor}                        from './task';

export async function invalidateCache(): Promise<void> {
    await pMkDir(OUTPUT_DIR, {recursive: true});
    await pMkDir(CACHE_DIR, {recursive: true});

    const cacheConstFile = `${CACHE_DIR}/constant.json`;

    let valid = existsSync(cacheConstFile);

    const programConst = computeProgramConstants();
    if (valid) {
        const cacheConst = (await pReadFile(cacheConstFile))
            .toString();
        valid            = programConst === cacheConst;
    }

    if (!valid) {
        console.log('Cache invalid, clearing cache...');
        await pEmptyDir(CACHE_DIR);
        await pEmptyDir(OUTPUT_DIR);
        await pWriteFile(cacheConstFile, programConst);
    }
}

function computeProgramConstants(): string {
    const props = {
        shinglingSize: SHINGLING_SIZE,
        queryOffsetMs: QUERY_OFFSET_MS,
        queryFiles: QUERY_FILES,
        maxSessionLength: MAX_SESSION_LENGTH,
    };
    return JSON.stringify(props);
}

/**
 * Cache handler for a string
 */
export const stringCacheHandler: TaskCacheDescriptor<string> = {
    load: async (filename: string) => (await pReadFile(filename)).toString(),
    save: (filename: string, result: string) => pWriteFile(filename, result),
};

/**
 * Cache handler for a number
 */
export const numberCacheHandler: TaskCacheDescriptor<number> = {
    load: async (filename: string) => parseFloat((await pReadFile(filename)).toString()),
    save: (filename: string, result: number) => pWriteFile(filename, result.toString()),
};

/**
 * Cache handler for an array buffer
 */
export const arrayBufferCacheHandler: TaskCacheDescriptor<ArrayBuffer> = {
    load: async (filename: string) => {
        const metaFileContents = JSON.parse((readFileSync(filename + '.json')).toString());
        const arrayBuffer      = new ArrayBuffer(metaFileContents.byteLength * MATRIX_CELL_SIZE);
        let byteOffset         = 0;
        for (let i = 0; i < metaFileContents.files; i++) {
            await new Promise((resolve: () => void) => {
                createReadStream(`${filename}.${i}`)
                    .on('data', (data) => {
                        //const block_size = 1024 * 1024;
                        //while (byteOffset < metaFileContents.byteLength * MATRIX_CELL_SIZE) {
                        //const data = Buffer.alloc(Math.min(block_size, metaFileContents.byteLength * MATRIX_CELL_SIZE
                        // - byteOffset));
                        const view = new Uint8Array(arrayBuffer, byteOffset, data.byteLength);
                        //console.log(`i: ${i}, bo: ${byteOffset}, dl: ${data.byteLength},dl+bo: ${byteOffset +
                        // data.byteLength}, tl: ${metaFileContents.byteLength * MATRIX_CELL_SIZE}`)
                        view.set(new Uint8Array(data));
                        byteOffset += data.byteLength;
                        //}
                    })
                    .on('end', () => resolve());
            });
        }
        return Promise.resolve(arrayBuffer);
    },
    save: async (filename: string, result: ArrayBuffer) => {
        const metaFileContents = {
            files: Math.ceil(result.byteLength / BUFFER_MAX_LENGTH),
            byteLength: result.byteLength / MATRIX_CELL_SIZE,
        };
        await pWriteFile(filename + '.json', JSON.stringify(metaFileContents));

        for (let offset = 0; offset < result.byteLength; offset += BUFFER_MAX_LENGTH) {
            const buffer = Buffer.from(result, offset, Math.min(BUFFER_MAX_LENGTH, result.byteLength - offset));
            await pWriteFile(`${filename}.${offset / BUFFER_MAX_LENGTH}`, buffer);
        }
    },
};

/**
 * Cache handler for json
 */
export function jsonCacheHandler(taskname: string, type: 'task' | 'output' = 'task', noLoad?: boolean): TaskCacheDescriptor<any> {
    return {
        type,
        file: taskname + '.json',
        noLoad,
        load: async (filename: string) => JSON.parse((await pReadFile(filename)).toString()),
        save: async (filename: string, result: any) => pWriteFile(filename, JSON.stringify(result)),
    };
};
