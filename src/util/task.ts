import {existsSync}                   from 'fs';
import * as ProgressBar               from 'progress';
import {identity}                     from 'rxjs';
import {CACHE_DIR, OUTPUT_DIR, TASKS} from '../constant';
import {fixedStringWidth}             from './util';

/**
 * A map of results from different tasks
 */
export type DependencyResultMap = { [key: string]: any }

/**
 * A task interface. Allows for tasks to be cached as well
 */
export interface TaskDescriptor<T> {
    name: string;
    title: string;
    deps: string[];
    totalTicks: number;
    resultTransformer: (t: T) => any;
    compute: (bar: ProgressBar, depResults: { [key: string]: any }) => Promise<T>;

    cache?: TaskCacheDescriptor<T>;
}

/**
 * A task caching interface. Can be optionally used in any TaskDescriptor
 */
export interface TaskCacheDescriptor<T> {
    type?: 'task' | 'output';
    file?: string;
    noLoad?: boolean;
    load: (fname: string) => Promise<T | null>;
    save: (fname: string, result: T) => Promise<void>;
}

const taskMap    = new Map<string, () => any>();
const resultMap  = new Map<string, Promise<any>>();
const depNameMap = new Map<string, string[]>();

/**
 * Register a new task using a task descriptor.
 * @param name The (system) name of the task
 * @param title The (human) title of the task
 * @param deps Dependencies, as task names
 * @param totalTicks The total number of ticks in this task
 * @param compute The task runner function
 * @param resultTransformer A stringification function to output the result
 * @param cache A cache descriptor
 */
export function task<T>({name, title, deps, totalTicks, compute, resultTransformer, cache}: TaskDescriptor<T>) {
    depNameMap.set(name, deps);
    // Register the new task in the task runner map
    taskMap.set(name, async () => {
        title             = fixedStringWidth(title, 64);
        resultTransformer = resultTransformer || identity;

        // Try to load cache if it exists
        const loadResult = await checkCache(name, cache);
        if (loadResult !== null || typeof loadResult !== 'object') {
            await taskReady(name, title, loadResult, true, resultTransformer, cache);
            return loadResult;
        }

        // Else, resolve dependencies and run task
        const resolvedDependencies = await resolve(deps);
        const progressBar          = createProgressBar(title, totalTicks);
        const result               = await compute(progressBar, resolvedDependencies);

        await taskReady(name, title, result, false, resultTransformer, cache);
        return result;
    });
}

/**
 * Resolve task dependencies
 *
 * @param deps The task dependencies as an array of their names
 */
async function resolve(deps: string[]): Promise<{ [key: string]: any }> {
    const obj: { [key: string]: any } = {};
    // Loop over all dependency names
    const depPromises                 = deps.map(async (dep: string) => {
        // If the result already exists (or is being created), return it
        const depResult = resultMap.get(dep);
        if (depResult !== undefined)
            return [dep, await depResult] as [string, any];

        // Check if the task exists
        const depTask = taskMap.get(dep);
        if (depTask === undefined)
            throw new Error(`Dependency does not exist: ${dep}`);

        // Run the task, set it in the result map, and return the result
        const taskPromise = depTask();
        resultMap.set(dep, taskPromise);
        return [dep, await taskPromise] as [string, any];
    });

    // Await all promises, and map the results into the return object
    const depResults = await Promise.all(depPromises);
    depResults.forEach(([dep, result]: [string, any]) => obj[dep] = result);
    return obj;
}

/**
 * Start the program by running the top-level output tasks
 */
export function start(): Map<string, Promise<any>> {
    resolve(TASKS);
    return resultMap;
}

/**
 * Check whether a task has a valid cache. If so, load the cache.
 *
 * @param taskName The name of the task
 * @param cache The cache descriptor
 */
async function checkCache<T>(taskName: string, cache?: TaskCacheDescriptor<T>): Promise<T | null> {
    if (cache === undefined || cache.noLoad)
        return null;
    const filename = cacheFileName(taskName, cache);
    if (!existsSync(filename))
        return null;
    return cache.load(filename);
}

/**
 * Create a progress bar entity for a task
 *
 * @param taskName The name of the task
 * @param ticks The number of ticks in this task
 */
function createProgressBar(taskName: string, ticks: number): ProgressBar {
    const progressBarFormat = '{} [:bar] :percent :elapseds - :etas';

    return new ProgressBar(progressBarFormat.replace('{}', taskName), {
        total: ticks,
        width: 32,
        renderThrottle: 1000,
        incomplete: ' ',
        clear: true,
    });
}

/**
 * Compute the filename from a task name and cache descriptor.
 * It resolves the correct directory (cache / output directory)
 * and the correct file name (cache descriptor filename or task name)
 *
 * @param taskName The name of the task
 * @param cache The cache descriptor
 */
function cacheFileName<T>(taskName: string, cache: TaskCacheDescriptor<T>): string {
    cache.type      = cache.type || 'task';
    const cacheDir  = cache.type === 'task' ? CACHE_DIR : OUTPUT_DIR;
    const cacheFile = cache.file || taskName;
    return `${cacheDir}/${cacheFile}`;
}

/**
 * Handle a done task. It handles console output, and optional caching.
 *
 * @param name
 * @param title
 * @param result
 * @param fromCache
 * @param resultTransformer
 * @param cache
 */
async function taskReady<T>(name: string, title: string, result: T, fromCache: boolean,
                            resultTransformer: (t: T) => any,
                            cache?: TaskCacheDescriptor<T>) {
    let transformedResult = resultTransformer(result);
    transformedResult     = transformedResult === null ? 'Done.' : transformedResult;
    console.log(`${title} ${transformedResult} ${fromCache ? '(Cached)' : ''}`);
    if (cache && !fromCache) {
        const filename = cacheFileName(name, cache);
        await cache.save(filename, result);
    }
}

export function printDependencies() {
    console.log('Dependencies:');
    for (const [name, value] of depNameMap) {
        console.log(`${fixedStringWidth(name, 24)}: ${value.join(', ')}`);
    }
}