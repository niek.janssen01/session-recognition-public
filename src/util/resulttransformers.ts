import {round} from './util';

export const NoPrintableResult = () => null;

export const percentage = (n: number) => `${round(n, 2)}%`;
